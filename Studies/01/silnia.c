#include <stdio.h>

int silnia (int n){
	if (n==0) return 1;
	else return n*silnia(n-1);
}

int main(){
	int n;
	printf("Podaj liczbe, ktorej silnie program obliczy.\n");
	scanf("%d",&n);
	printf("\n");
	printf("Silnia liczby %d jest rowna %d\n",n,silnia(n));
	return 0;
}
