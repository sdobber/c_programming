#include <stdio.h>
//#include <string.h>

int main(){

    // definicja typu struktury:
    struct osoba{
        char Imie[30];
        char Nazwisko[40];
        int wiek;
    };
    //deklaracja zmiennej:
    struct osoba os1, os2;

    //przypisanie warto?ci:
    strcpy(os1.Imie,"Ewa");
    strcpy(os1.Nazwisko,"Rajska");
    os1.wiek=20;

    strcpy(os2.Imie,"Adam");
    strcpy(os2.Nazwisko,"Rajski");
    os2.wiek=22;
    //os1.klasa=2;
    //os1.srednia=4.5;
    printf("%s %s %d\n", os1.Imie, os1.Nazwisko, os1.wiek);
    printf("%s %s %d", os2.Imie, os2.Nazwisko, os2.wiek);

    return 0;
}
