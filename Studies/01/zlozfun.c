#include <stdio.h>
#include <math.h>

double fun(double x){
	return x*x;
}

double fun2(double y, int k){
	int i;
	for (i=0;i<k;i++){
		y=fun(y);
	}
	return y;
}

int main(){
	int k;
	printf("Podaj liczbe.\n");
	scanf("%d",&k);
	printf("\n");
	printf("%3.2lf",fun2(2,k));
	return 0;
}
