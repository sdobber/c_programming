#include <time.h>
#include <stdio.h>
#include <stdlib.h>

void losuj (int tab[20][20]){
	int i,r,j,zarodek;
	zarodek=time(NULL);
	srand(zarodek);
	for(i=0;i<20;i++){
		for(j=0;j<20;j++){
			r=(int)(rand()/(RAND_MAX+1.0)*100.0);
			if (r>=70){
				tab[i][j]=1;
			}
			else{
				tab[i][j]=0;
			}
		}
	}
}


void rysuj (int tab[20][20]){
	int i,j;
	for(i=0;i<20;i++){
		for(j=0;j<20;j++){
			if (tab[i][j]==1){
				printf("*");
			}
			if (tab[i][j]==0){
				printf(" ");
			}
		}
		printf("\n");
	}
}


int liczba_sasiadow(int i, int j, int tab[20][20]){

          
if (i==0 && j == 0) return (tab[0][1] + tab[1][0]+ tab[1][1]);         
if (i==0 && j == 19) return (tab[0][18]+ tab[1][18] + tab[1][19]);
if (j==0 && i == 19) return (tab[18][0] + tab[18][1] + tab[19][1]);
if (i==19 && j == 19) return (tab[18][19] + tab[19][18] + tab[18][18]);
if (j==0 && i != 0 && i!= 19) return (tab[i][j+1] + tab[i+1][j] + tab[i+1][j+1] + tab[i-1][j] + tab[i-1][j-1]);
if (j==19 && i !=0  && i!=19) return (tab[i-1][j-1] + tab[i-1][j] + tab[i][j-1]+ tab[i+1][j-1] + tab[i+1][j]);
if (i==0 && j !=0 && j!= 19) return (tab[i][j-1] + tab[i][j+1] + tab[i+1][j-1] + tab[i+1][j] + tab[i+1][j+1]);
if (i==19 && j != 0  && j!=19) return (tab[i-1][j-1]+ tab[i-1][j]+ tab[i-1][j+1] + tab[i][j-1]+ tab[i][j+1]);
if ( (0 < i) && (i < 19) && (0< j) && (j< 19)) return (tab[i-1][j-1]+ tab[i-1][j]+ tab[i-1][j+1] + tab[i][j-1]+ tab[i][j+1] +
                                                 tab[i+1][j-1] + tab[i+1][j] + tab[i+1][j+1]);
}


void zycie(int tab[20][20]){
	int i,j,s;
	int tab_pom[20][20];
	for(i=0;i<20;i++){
		for(j=0;j<20;j++){
			s=liczba_sasiadow(i,j,tab);
			if(s==3) tab_pom[i][j]=1;
			if(s==2) tab_pom[i][j]=tab[i][j];
			if(s!=2 && s!=3) tab_pom[i][j]=0;
		}
	}
for(i=0;i<20;i++){
		for(j=0;j<20;j++){
			tab[i][j]=tab_pom[i][j];
		}
}
}


int main(){
	int i,j;
	int tab[20][20];
	losuj(tab);
	rysuj(tab);
	for(i=0;i<1000;i++){
		zycie(tab);
		rysuj(tab);
		getchar();
	}
}
