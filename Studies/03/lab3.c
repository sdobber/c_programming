#include <stdio.h>
#include <stdlib.h>
int main(){
	int i,a,d=0,licznik=0;
	char *p;
	printf("Podaj liczbe mniejsza niz 1048576\n");
	scanf("%d", &a);
	if (a<=0 || a>1048576){
		printf("Too much or too little memory requested\n");
		return 0;
	}
	else{
		p = (char *) malloc(a*sizeof(char));
		if(p!=NULL){
			for(i=0; i<a; i++){
				p[i]='A'+d;
				d++;
				if(d==26) d=0;
			}
			if(a>400){
				for(i=0; i<400;i++){
					printf("%c", p[i]);
					licznik++;
					if(licznik%40==0) printf("\n");
				}
			}
			else{
				for(i=0; i<a;i++){
					printf("%c", p[i]);
					licznik++;
					if(licznik%40==0) printf("\n");
				}
			}
		}
		else{
			printf("Allocation failed\n");
			return 0;	
		}
	}
	free(p);
	return 0;
}
