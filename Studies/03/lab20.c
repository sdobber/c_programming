#include <stdio.h>
int main (){
	int *p,*s,tab[5]={0,1,2,3,4}, i;//tworzymy dwa wskazniki do int, tablice int, i zmienna int
	p=&tab[0];//pod wskaznik p podstawiamy adres pierwszego elementu tablicy
	s=&tab[4];//pod wskaznik s podstawiamy adres ostatniego elementu tablicy
	i=s-p;//odejmujemy wskazniki
	printf("%d %p %p",i,(void *)&tab[0],(void *)&tab[4]);//wyswietlamy wynik (4 elementy tablicy) i pierwszy i ostatni adres w tablicy
	//printf("\n%d %p",*p,p);
	return 0;
}
