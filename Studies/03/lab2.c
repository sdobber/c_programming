#include <stdio.h>
#include <errno.h>

int main(int argc, char *argv[]){
	FILE *source, *destination;
	int character;
	char letter='a';
	int tab[26];
	int whitespace=0, lines=0, all=0;
	int i;
	if(argc!=3){
		printf("Podaj nazwe programu, zrodlo i cel\n");
		return 1;
	}
	if((source=fopen(argv[1], "rt"))==NULL){
		printf("Nie mozna otworzyc %s\n", argv[1]);
		return 2;
	}
	if((destination=fopen(argv[2], "wt"))==NULL){
		printf("Nie mozna otworzyc %s\n", argv[2]);
		fclose(source);
		return 3;
	}
	for(i=0; i<26; i++){
		tab[i]=0;
	}
	while((character=fgetc(source)) != EOF){
		all++;
		if (character==10){
			lines++;
		}
		else if(character==9 || character==32){
			whitespace++;
		}
		else if(character>=97 && character<=122){
			tab[character-97]++;
		}
	}
	fclose(source);
	fprintf(destination, "Lines: %d\n", lines+1);
	fprintf(destination, "Whitespaces: %d\n", whitespace);
	fprintf(destination, "Characters: %d\n", all);
	for(i=0; i<26; i++){
		fprintf(destination, "Small letter: %c : %d\n", letter, tab[i]);
		letter++;
	}
    fclose(destination);
	return 0;
}
