#include <stdio.h>
#include <stdlib.h>
struct element{
	int value;
	struct element *next;
};
int main(void){
	int values[10] = { 2, 4, 5, 6, 7, 8, 9, 1, 3, 0}, i;
	
	struct element *head, *tail, *temp;
	head= (struct element *) malloc(sizeof(struct element));
	if(head!=NULL){
		head->next=NULL;
		head->value=values[0];
		tail=head;
		for(i=0; i<10; i++){
			tail->next = (struct element *) malloc(sizeof(struct element));
			tail=tail->next;
			tail->next=NULL;
			tail->value=values[i+1];
		}
		temp = head;
		printf("First 5 values\n");
		while((temp->value)!=values[5]){
			printf("%d\n", temp->value);
			temp=temp->next;
		}
		temp=head;
		printf("First 7 values\n");
		while((temp->value)!=values[7]){
			printf("%d\n", temp->value);
			temp=temp->next;
		}
	}
	else{
		printf("Allocation failed\n");
		return 0;
	}
	free(head);
	free(temp);
	free(tail);
	printf("Memory is freed\n");
	return 0;
}
