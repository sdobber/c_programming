#include <stdio.h>
#include <stdlib.h>
int main (){
	int **p,a,i,j;
	printf("Podaj rzad macierzy\n");
	scanf("%d",&a);
	if(a>20){
		printf("Matrix too big\n");
		return 0;
	}
	else{
		p=(int **) malloc(a*sizeof(int *));
		if(p!=NULL){
			for(i=0; i<a; i++){
				p[i]=(int *) malloc(a*sizeof(int));
			}
			for(i=0; i<a; i++){
				for(j=0; j<a; j++){
					p[i][j]=(i+1)*(j+1);
				}
			}
			printf(" ");
			for(i=0; i<a; i++) printf("%6d",i+1);
			printf("\n");
			for(i=0; i<a; i++){
				printf("%d",i+1);
				for(j=0; j<a; j++){
					printf("%6d",p[i][j]);
				}
				printf("\n");
			}
		}
		else{
			printf("Allocation failed\n");
			return 0;
		}
	}
	free(p);
	return 0;
}
