#include <stdio.h>
#include <errno.h>

int main(int argc, char *argv[]){
	FILE *source, *destination;
	char line[128];//bufor jednej linii
	int i=0;
	if(argc!=3){
		printf("Podaj nazwe programu, zrodlo i cel\n");
		return 1;
	}
	if((source=fopen(argv[1], "rt"))==NULL){
		printf("Nie mozna otworzyc %s\n", argv[1]);
		return 2;
	}
	if((destination=fopen(argv[2], "wt"))==NULL){
		printf("Nie mozna otworzyc %s\n", argv[2]);
		fclose(source);
		return 3;
	}
	while((fgets(line,sizeof(line),source))!=NULL){ //pobiera tekst wielkosci jednej linii
    	i++;
		fprintf(destination, "%d ", i);
		if(fputs(line,destination)==EOF)
    		break;
	}
    fclose(source);
    fclose(destination);
	return 0;
}
